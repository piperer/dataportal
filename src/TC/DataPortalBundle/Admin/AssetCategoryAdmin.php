<?php

namespace TC\DataPortalBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AssetCategoryAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('code')
            ->add('createdAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('code')
            ->add('createdAt')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            //->add('id')
            ->add('name')
            ->add('code')
            ->add('kpicollections','sonata_type_collection',
     array(
         'required' => false,
         'by_reference' => false
     ),
     array(
         'edit' => 'inline',
         'inline' => 'table',
         'sortable' => 'pos',
         'allow_delete' => true
     )
)    
        //    ->add('createdAt')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('code')
            ->add('createdAt')
        ;
    }
    
    
    public function prePersist($assetCategory)
    {
        
        foreach ($assetCategory->getKpicollections() as $kpicollection) {
            $kpicollection->setAssetcategory($assetCategory);
            
        }
    }

    
    public function preUpdate($assetCategory)
    {
        
        foreach ($assetCategory->getKpicollections() as $kpicollection) {
            $kpicollection->setAssetcategory($assetCategory);
            
        }
    }
}
