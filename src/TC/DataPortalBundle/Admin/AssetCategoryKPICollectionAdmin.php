<?php

namespace TC\DataPortalBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AssetCategoryKPICollectionAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('weightage')
            ->add('createdAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('assetcategory')
            ->add('kpi')
            ->add('isstarred')    
            ->add('weightage')
            ->add('options')    
            ->add('createdAt')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        
        
        $formMapper
          //  ->add('id')
            ->add('name')
            ->add('assetcategory')
            ->add('kpi')
            ->add('icon', 'sonata_type_model_list',
                    array('required' => false), 
                    array('link_parameters' => array('context' => 'default')))
            ->add('isStarred')
            ->add('weightage')
            ->add('controltype')    
            ->add('options','sonata_type_collection',
     array(
         'required' => false,
         'by_reference' => false
     ),
     array(
         'edit' => 'inline',
         'inline' => 'table',
         'allow_delete' => true
     )
)    
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('assetcategory')
            ->add('kpi')
            ->add('weightage')
            ->add('createdAt')
        ;
    }
    
    public function prePersist($ackpi)
    {
        
        foreach ($ackpi->getOptions() as $option ) {
            $option->setKpicollection($ackpi);
            
        }
    }

    
    public function preUpdate($ackpi)
    {
        
        foreach ($ackpi->getOptions() as $option) {
            $option->setKpicollection($ackpi);
            
        }
    }
}
