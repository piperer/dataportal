<?php

namespace TC\DataPortalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use TC\DataPortalBundle\Entity\AssetReport;
use Symfony\Component\HttpFoundation\JsonResponse;


class DefaultController extends Controller
{
    public function indexAction($assetCode)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $asset = $em->getRepository('TCDataPortalBundle:Asset')
                ->findOneByCode($assetCode);
        
        
        $kpis = $asset->getAssetcategory()->getKpicollections();
        
        $date = date('d-m-Y H:i:s');
        
        return $this->render('TCDataPortalBundle:Default:index.html.twig',
                array(
                    'date' => $date,
                    'asset'=> $asset,
                    'kpis' => $kpis
                ));
    }
    
    public function savedAction($assetCode) {
        
        $em = $this->getDoctrine()->getEntityManager();
        $asset = $em->getRepository('TCDataPortalBundle:Asset')
                ->findOneByCode($assetCode);
        
        
        
        if($this->getRequest()->isMethod('POST')){
            
            $data = $this->get('request')->request->get('kpi');
             foreach($data as $key=>$value){
               $assetReport = new AssetReport();
               $kpi = $em->getRepository('TCDataPortalBundle:AssetCategoryKPICollection')
                       ->find($key);
               $assetReport->setAsset($asset);
               $assetReport->setKpicollection($kpi);
               if(is_numeric($value)){
                   $assetReport->setWeightageResults($value);
               } else {
                   
                   if($value == 'yes'){
                    $assetReport->setWeightageResults(5);   
                   }
                   else{
                     $assetReport->setWeightageResults(0);   
                   }
                   
               }
               $assetReport->setUserId(1);
               $em->persist($assetReport);
               $em->flush();
               
             }
            
        }
        
         return new Response('Your inspection details have been saved');
        
    }
    
    public function getKPIJsonAction($assetCategoryCode, $auth) {
        
        if($auth != 'doodleblue'){
            $response = new Response();
            return $response->setContent('Not authorized');
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $assetCategory = $em->getRepository('TCDataPortalBundle:AssetCategory')
                ->findOneByCode($assetCategoryCode);
        
        $kpiCollection = $assetCategory->getKpicollections();
        
        $results = array();
        $results['id'] = $assetCategory->getId();
        $results ['name'] = $assetCategory->getName();
        $results ['code'] = $assetCategory->getCode();
        
        foreach ($kpiCollection as $key => $kpi) {
            
            $results['kpis'][$key]['id'] = $kpi->getId();
            $results['kpis'][$key]['name'] = $kpi->getName();
            $results['kpis'][$key]['description'] = $kpi->getKpi()->getDescription();
            //$results['kpis'][$key]['weightage'] = $kpi->getWeightage();
            
            $options = $kpi->getOptions();
            $optionsArray = array();
            $optionsScoresArray = array();
            foreach($options as $k=>$option){
                $optionsArray[$k]['name'] = $option->getName();
                $optionsArray[$k]['score'] = $option->getWeights();
                $optionsScoresArray[] = $option->getWeights();
                
            }
            //sort($optionsScoresArray);
            $results['kpis'][$key]['optionScores'] = $optionsScoresArray;
            $results['kpis'][$key]['options'] = $optionsArray;
        }
    //    echo '<pre>';        print_r($results); exit;
        return new JsonResponse($results);
    }
    
    public function postKPIJsonAction()
    {
        
    }
}
