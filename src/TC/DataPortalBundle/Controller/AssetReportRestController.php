<?php

namespace TC\DataPortalBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use TC\DataPortalBundle\Entity\AssetReport;
use Symfony\Component\HttpFoundation\Response;
use TC\DataPortalBundle\Entity\SecondaryAsset;
use CrEOF\Spatial\PHP\Types\Geometry\Point as Point;
use Application\Sonata\MediaBundle\Entity\Media;
use TC\DataPortalBundle\Entity\Option;

/**
 * AssetReport controller.
 *
 */
class AssetReportRestController extends FOSRestController
{

    /**
 * Collection get action
 * @var Request $request
 * @return array
 *
 * @Rest\View()
 */
public function getCategoriesAction(Request $request)
{
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('TCDataPortalBundle:AssetCategory')->findAll();

    return array(
        'entities' => $entities,
    );
}

public function getCategoryAction($code)
{
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('TCDataPortalBundle:AssetCategory')->findOneByCode($code);
    $category = $this->treatEntity($entity);
    
    
    return  $category;
   
}

private function treatEntity($entity)
{
    $kpicollections = $entity->getKpicollections();
    $provider = $this->container->get('sonata.media.provider.image');
    foreach($kpicollections as $collection){
        $entity->removeKpicollection($collection);
        if($collection->getIcon()){
        
        $publicUrl = $provider->generatePublicUrl($collection->getIcon(),'big')  ;
        
        } else {
            $publicUrl = 'TEST';
        }
        
        $collection->setIconPath($publicUrl);
        foreach($collection->getOptions() as $option){
           
            if($option->getIcon()){
                $url = $provider->generatePublicUrl($option->getIcon(),'big')  ;
            } else {
               $url = 'test' ;
            }
            
            $option->setIconPath($url);
           // $collection->addOption($option);
        }
        $entity->addKpicollection($collection);
       
        
    }
   // exit;
    return $entity;
}        

public function postAssetReportAction(Request $request)
{
    
    $entity = new AssetReport();
    $asset = new SecondaryAsset();
    $lat = $request->request->get('latitude');
     $long =     $request->request->get('longitude');
     $name =      $request->request->get('name');
    $location = new Point($long, $lat);
    $asset->setName($name);
    $asset->setLocation($location);
     $em = $this->getDoctrine()->getManager();
    $category = $em->getRepository('TCDataPortalBundle:AssetCategory')
            ->findOneByCode($request->request->get('category'));

    $asset->setAssetcategory($category);
    $createdAt = new \DateTime();
    $asset->setCreatedAt($createdAt);
    $em->persist($asset);
     $em->flush();
    
    $entity->setWeightageResults($request->request->get('weightageResults'));
   // $time = new \DateTime();
    $entity->setUserId($request->request->get('userId'));
    $time = new \DateTime($request->request->get('dateOfInspection'));
    $entity->setDateOfInspection($time);
    $entity->setDeviceId($request->request->get('deviceId'));
    $entity->setSecondaryAsset($asset);
    
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

      $response = new Response();
      $response->setContent("Report Created");
            $response->setStatusCode('201');
            return $response;
    

                
    
   
}        
}
