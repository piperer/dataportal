<?php

namespace TC\DataPortalBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use TC\DataPortalBundle\Entity\AssetReport;
use TC\DataPortalBundle\Form\AssetReportType;

/**
 * AssetReport controller.
 *
 */
class AssetReportController extends Controller
{

    /**
     * Lists all AssetReport entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TCDataPortalBundle:AssetReport')->findAll();

        return $this->render('TCDataPortalBundle:AssetReport:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new AssetReport entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new AssetReport();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('assetreport_show', array('id' => $entity->getId())));
        }

        return $this->render('TCDataPortalBundle:AssetReport:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a AssetReport entity.
     *
     * @param AssetReport $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AssetReport $entity)
    {
        $form = $this->createForm(new AssetReportType(), $entity, array(
            'action' => $this->generateUrl('assetreport_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new AssetReport entity.
     *
     */
    public function newAction()
    {
        $entity = new AssetReport();
        $form   = $this->createCreateForm($entity);

        return $this->render('TCDataPortalBundle:AssetReport:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a AssetReport entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TCDataPortalBundle:AssetReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AssetReport entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TCDataPortalBundle:AssetReport:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing AssetReport entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TCDataPortalBundle:AssetReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AssetReport entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TCDataPortalBundle:AssetReport:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a AssetReport entity.
    *
    * @param AssetReport $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(AssetReport $entity)
    {
        $form = $this->createForm(new AssetReportType(), $entity, array(
            'action' => $this->generateUrl('assetreport_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing AssetReport entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TCDataPortalBundle:AssetReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AssetReport entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('assetreport_edit', array('id' => $id)));
        }

        return $this->render('TCDataPortalBundle:AssetReport:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a AssetReport entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TCDataPortalBundle:AssetReport')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AssetReport entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('assetreport'));
    }

    /**
     * Creates a form to delete a AssetReport entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('assetreport_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    
    public function reportAction()
    {
        $admin_pool = $this->get('sonata.admin.pool');
        
        return $this->render('TCDataPortalBundle:AssetReport:report.html.twig', array(
            'admin_pool' => $admin_pool
        ));
       
    }
}
