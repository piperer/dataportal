<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SecondaryAsset
 */
class SecondaryAsset
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var point
     */
    private $location;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \TC\DataPortalBundle\Entity\AssetCategory
     */
    private $assetcategory;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SecondaryAsset
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set location
     *
     * @param point $location
     * @return SecondaryAsset
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return point 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return SecondaryAsset
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set assetcategory
     *
     * @param \TC\DataPortalBundle\Entity\AssetCategory $assetcategory
     * @return SecondaryAsset
     */
    public function setAssetcategory(\TC\DataPortalBundle\Entity\AssetCategory $assetcategory = null)
    {
        $this->assetcategory = $assetcategory;

        return $this;
    }

    /**
     * Get assetcategory
     *
     * @return \TC\DataPortalBundle\Entity\AssetCategory 
     */
    public function getAssetcategory()
    {
        return $this->assetcategory;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        // Add your code here
    }
}
