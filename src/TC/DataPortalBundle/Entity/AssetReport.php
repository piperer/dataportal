<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AssetReport
 */
class AssetReport
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $assetId;

    
    /**
     * @var integer
     */
    private $weightageResults;

    /**
     * @var \DateTime
     */
    private $dateOfInspection;

    /**
     * @var integer
     */
    private $userId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set assetId
     *
     * @param integer $assetId
     * @return AssetReport
     */
    public function setAssetId($assetId)
    {
        $this->assetId = $assetId;

        return $this;
    }

    /**
     * Get assetId
     *
     * @return integer 
     */
    public function getAssetId()
    {
        return $this->assetId;
    }

    

    /**
     * Set weightageResults
     *
     * @param integer $weightageResults
     * @return AssetReport
     */
    public function setWeightageResults($weightageResults)
    {
        $this->weightageResults = $weightageResults;

        return $this;
    }

    /**
     * Get weightageResults
     *
     * @return integer 
     */
    public function getWeightageResults()
    {
        return $this->weightageResults;
    }

    /**
     * Set dateOfInspection
     *
     * @param \DateTime $dateOfInspection
     * @return AssetReport
     */
    public function setDateOfInspection($dateOfInspection)
    {
        $this->dateOfInspection = $dateOfInspection;

        return $this;
    }

    /**
     * Get dateOfInspection
     *
     * @return \DateTime 
     */
    public function getDateOfInspection()
    {
        return $this->dateOfInspection;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return AssetReport
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
    /**
     * @var \TC\DataPortalBundle\Entity\SecondaryAsset
     */
    private $asset;

    

    /**
     * Set asset
     *
     * @param \TC\DataPortalBundle\Entity\SecondaryAsset $asset
     * @return AssetReport
     */
    public function setAsset(\TC\DataPortalBundle\Entity\Asset $asset = null)
    {
        $this->asset = $asset;

        return $this;
    }

    /**
     * Get asset
     *
     * @return \TC\DataPortalBundle\Entity\Asset 
     */
    public function getAsset()
    {
        return $this->asset;
    }

    
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if(!$this->getDateOfInspection()) {
            $this->dateOfInspection = new \DateTime();
        }
    }
    /**
     * @var integer
     */
    private $deviceId;


    /**
     * Set deviceId
     *
     * @param integer $deviceId
     * @return AssetReport
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    /**
     * Get deviceId
     *
     * @return integer 
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }
    /**
     * @var \TC\DataPortalBundle\Entity\SecondaryAsset
     */
    private $secondaryAsset;


    /**
     * Set secondaryAsset
     *
     * @param \TC\DataPortalBundle\Entity\SecondaryAsset $secondaryAsset
     * @return AssetReport
     */
    public function setSecondaryAsset(\TC\DataPortalBundle\Entity\SecondaryAsset $secondaryAsset = null)
    {
        $this->secondaryAsset = $secondaryAsset;

        return $this;
    }

    /**
     * Get secondaryAsset
     *
     * @return \TC\DataPortalBundle\Entity\SecondaryAsset 
     */
    public function getSecondaryAsset()
    {
        return $this->secondaryAsset;
    }
}
