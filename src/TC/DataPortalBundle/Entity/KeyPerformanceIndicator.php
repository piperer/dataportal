<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * KeyPerformanceIndicator
 */
class KeyPerformanceIndicator
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    
    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return KeyPerformanceIndicator
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    

    /**
     * Set description
     *
     * @param string $description
     * @return KeyPerformanceIndicator
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return KeyPerformanceIndicator
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    

    

    /**
     * @var \TC\DataPortalBundle\Entity\ControlType
     */
    private $controltype;


    

    

    /**
     * Set controltype
     *
     * @param \TC\DataPortalBundle\Entity\ControlType $controltype
     * @return KeyPerformanceIndicator
     */
    public function setControltype(\TC\DataPortalBundle\Entity\ControlType $controltype = null)
    {
        $this->controltype = $controltype;

        return $this;
    }

    /**
     * Get controltype
     *
     * @return \TC\DataPortalBundle\Entity\ControlType 
     */
    public function getControltype()
    {
        return $this->controltype;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $assetcategorycollection;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->assetcategorycollection = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add assetcategorycollection
     *
     * @param \TC\DataPortalBundle\Entity\AssetCategoryKPICollection $assetcategorycollection
     * @return KeyPerformanceIndicator
     */
    public function addAssetcategorycollection(\TC\DataPortalBundle\Entity\AssetCategoryKPICollection $assetcategorycollection)
    {
        $this->assetcategorycollection[] = $assetcategorycollection;

        return $this;
    }

    /**
     * Remove assetcategorycollection
     *
     * @param \TC\DataPortalBundle\Entity\AssetCategoryKPICollection $assetcategorycollection
     */
    public function removeAssetcategorycollection(\TC\DataPortalBundle\Entity\AssetCategoryKPICollection $assetcategorycollection)
    {
        $this->assetcategorycollection->removeElement($assetcategorycollection);
    }

    /**
     * Get assetcategorycollection
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssetcategorycollection()
    {
        return $this->assetcategorycollection;
    }
    
    public function __toString()
    {
    	return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $options;


    /**
     * Add options
     *
     * @param \TC\DataPortalBundle\Entity\Option $options
     * @return KeyPerformanceIndicator
     */
    public function addOption(\TC\DataPortalBundle\Entity\Option $options)
    {
        $this->options[] = $options;

        return $this;
    }

    /**
     * Remove options
     *
     * @param \TC\DataPortalBundle\Entity\Option $options
     */
    public function removeOption(\TC\DataPortalBundle\Entity\Option $options)
    {
        $this->options->removeElement($options);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOptions()
    {
        return $this->options;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if(!$this->getCreatedAt()) {
            $this->createdAt = new \DateTime();
        }
    }
}
