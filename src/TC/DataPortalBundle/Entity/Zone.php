<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CrEOF\Spatial\PHP\Types\Geometry\Polygon as Polygon;
/**
 * Zone
 */
class Zone
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var polygon
     */
    private $location;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $wards;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->wards = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Zone
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Zone
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set location
     *
     * @param polygon $location
     * @return Zone
     */
    public function setLocation($location)
    {
    	$location = new Polygon(array(array(array(0,0),array(8,0),array(12,9),array(0,9),array(0,0)),array(array(5,3),array(4,5),array(7,9),array(3,7),array(5,3))));
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return polygon 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Zone
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add wards
     *
     * @param \TC\DataPortalBundle\Entity\Ward $wards
     * @return Zone
     */
    public function addWard(\TC\DataPortalBundle\Entity\Ward $wards)
    {
        $this->wards[] = $wards;

        return $this;
    }

    /**
     * Remove wards
     *
     * @param \TC\DataPortalBundle\Entity\Ward $wards
     */
    public function removeWard(\TC\DataPortalBundle\Entity\Ward $wards)
    {
        $this->wards->removeElement($wards);
    }

    /**
     * Get wards
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWards()
    {
        return $this->wards;
    }
    
    public function __toString()
    {
    	return $this->code;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if(!$this->getCreatedAt()) {
            $this->createdAt = new \DateTime();
        }
    }
}
