<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * @ORM\Entity
 * 
 * 
 * @ExclusionPolicy("all") 
 */
class Option
{
    /**
     * @var integer
     * 
     */
    private $id;

    /**
     * @var string
     * 
     * @Expose
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Option
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var \TC\DataPortalBundle\Entity\KeyPerformanceIndicator
     */
    private $kpi;


    /**
     * Set kpi
     *
     * @param \TC\DataPortalBundle\Entity\KeyPerformanceIndicator $kpi
     * @return Option
     */
    public function setKpi(\TC\DataPortalBundle\Entity\KeyPerformanceIndicator $kpi = null)
    {
        $this->kpi = $kpi;

        return $this;
    }

    /**
     * Get kpi
     *
     * @return \TC\DataPortalBundle\Entity\KeyPerformanceIndicator 
     */
    public function getKpi()
    {
        return $this->kpi;
    }
    
    public function __toString()
    {
    	return $this->name;
    }
    /**
     * @var integer
     * 
     * @Expose
     */
    private $weights;

    /**
     * @var \TC\DataPortalBundle\Entity\AssetCategoryKPICollection
     */
    private $kpicollection;


    /**
     * Set weights
     *
     * @param integer $weights
     * @return Option
     */
    public function setWeights($weights)
    {
        $this->weights = $weights;

        return $this;
    }

    /**
     * Get weights
     *
     * @return integer 
     */
    public function getWeights()
    {
        return $this->weights;
    }

    /**
     * Set kpicollection
     *
     * @param \TC\DataPortalBundle\Entity\AssetCategoryKPICollection $kpicollection
     * @return Option
     */
    public function setKpicollection(\TC\DataPortalBundle\Entity\AssetCategoryKPICollection $kpicollection = null)
    {
        $this->kpicollection = $kpicollection;

        return $this;
    }

    /**
     * Get kpicollection
     *
     * @return \TC\DataPortalBundle\Entity\AssetCategoryKPICollection 
     */
    public function getKpicollection()
    {
        return $this->kpicollection;
    }
    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     */
    private $icon;


    /**
     * Set icon
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $icon
     * @return Option
     */
    public function setIcon(\Application\Sonata\MediaBundle\Entity\Media $icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media 
     */
    public function getIcon()
    {
        return $this->icon;
    }
    
    /**
     * @var string
     * 
     * @Expose
     */
    private $iconPath;
    
    public function getIconPath()
    {
        return $this->iconPath;
    } 
    
    public function setIconPath($iconPath)
    {
        $this->iconPath = $iconPath;
    } 
}
