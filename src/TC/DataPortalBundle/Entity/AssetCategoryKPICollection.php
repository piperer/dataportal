<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;


/**
 * @ORM\Entity
 * 
 * 
 * @ExclusionPolicy("all") 
 */
class AssetCategoryKPICollection
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     * 
     * @Expose
     */
    private $name;

    /**
     * @var integer
     * 
     *
     */
    private $weightage;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \TC\DataPortalBundle\Entity\AssetCategory
     */
    private $assetcategory;

    /**
     * @var \TC\DataPortalBundle\Entity\KeyPerformanceIndicator
     */
    private $kpi;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AssetCategoryKPICollection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set weightage
     *
     * @param integer $weightage
     * @return AssetCategoryKPICollection
     */
    public function setWeightage($weightage)
    {
        $this->weightage = $weightage;

        return $this;
    }

    /**
     * Get weightage
     *
     * @return integer 
     */
    public function getWeightage()
    {
        return $this->weightage;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return AssetCategoryKPICollection
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set assetcategory
     *
     * @param \TC\DataPortalBundle\Entity\AssetCategory $assetcategory
     * @return AssetCategoryKPICollection
     */
    public function setAssetcategory(\TC\DataPortalBundle\Entity\AssetCategory $assetcategory = null)
    {
        $this->assetcategory = $assetcategory;

        return $this;
    }

    /**
     * Get assetcategory
     *
     * @return \TC\DataPortalBundle\Entity\AssetCategory 
     */
    public function getAssetcategory()
    {
        return $this->assetcategory;
    }

    /**
     * Set kpi
     *
     * @param \TC\DataPortalBundle\Entity\KeyPerformanceIndicator $kpi
     * @return AssetCategoryKPICollection
     */
    public function setKpi(\TC\DataPortalBundle\Entity\KeyPerformanceIndicator $kpi = null)
    {
        $this->kpi = $kpi;

        return $this;
    }

    /**
     * Get kpi
     *
     * @return \TC\DataPortalBundle\Entity\KeyPerformanceIndicator 
     */
    public function getKpi()
    {
        return $this->kpi;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if(!$this->getCreatedAt()) {
            $this->createdAt = new \DateTime();
        }
    }
    
    public function __toString()
    {
    	return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     * 
     * @Expose
     */
    private $options;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add options
     *
     * @param \TC\DataPortalBundle\Entity\Option $options
     * @return AssetCategoryKPICollection
     */
    public function addOption(\TC\DataPortalBundle\Entity\Option $options)
    {
        $this->options[] = $options;

        return $this;
    }

    /**
     * Remove options
     *
     * @param \TC\DataPortalBundle\Entity\Option $options
     */
    public function removeOption(\TC\DataPortalBundle\Entity\Option $options)
    {
        $this->options->removeElement($options);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOptions()
    {
        return $this->options;
    }
    /**
     * @var \TC\DataPortalBundle\Entity\ControlType
     */
    private $controltype;


    /**
     * Set controltype
     *
     * @param \TC\DataPortalBundle\Entity\ControlType $controltype
     * @return AssetCategoryKPICollection
     */
    public function setControltype(\TC\DataPortalBundle\Entity\ControlType $controltype = null)
    {
        $this->controltype = $controltype;

        return $this;
    }

    /**
     * Get controltype
     *
     * @return \TC\DataPortalBundle\Entity\ControlType 
     */
    public function getControltype()
    {
        return $this->controltype;
    }
    
    public function getOptionWeights()
    {
        $optionWeights = array();
        foreach($this->options as $option){
           $optionWeights[] = $option->getWeights(); 
        }
        sort($optionWeights);
        return json_encode($optionWeights);
    }        
    /**
     * @var boolean
     */
    private $isStarred;


    /**
     * Set isStarred
     *
     * @param boolean $isStarred
     * @return AssetCategoryKPICollection
     */
    public function setIsStarred($isStarred)
    {
        $this->isStarred = $isStarred;

        return $this;
    }

    /**
     * Get isStarred
     *
     * @return boolean 
     */
    public function getIsStarred()
    {
        return $this->isStarred;
    }
    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * 
     */
    private $icon;


    /**
     * Set icon
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $icon
     * @return AssetCategoryKPICollection
     */
    public function setIcon(\Application\Sonata\MediaBundle\Entity\Media $icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getIcon()
    {
        return $this->icon;
    }
    
    /**
     * @var string
     * 
     * @Expose
     */
    private $iconPath;
    
    public function getIconPath()
    {
        return $this->iconPath;
    } 
    
    public function setIconPath($iconPath)
    {
        $this->iconPath = $iconPath;
    } 
}
