<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * AssetReportRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AssetReportRepository extends EntityRepository
{
}
