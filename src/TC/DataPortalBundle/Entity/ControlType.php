<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ControlType
 */
class ControlType
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $divisions;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ControlType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set divisions
     *
     * @param integer $divisions
     * @return ControlType
     */
    public function setDivisions($divisions)
    {
        $this->divisions = $divisions;

        return $this;
    }

    /**
     * Get divisions
     *
     * @return integer 
     */
    public function getDivisions()
    {
        return $this->divisions;
    }
    
    public function __toString()
    {
    	return $this->name;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        // Add your code here
    }
}
