<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CrEOF\Spatial\PHP\Types\Geometry\Point as Point;

/**
 * Asset
 */
class Asset
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Asset
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Asset
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Asset
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @var \TC\DataPortalBundle\Entity\AssetCategory
     */
    private $assetcategory;


    /**
     * Set assetcategory
     *
     * @param \TC\DataPortalBundle\Entity\AssetCategory $assetcategory
     * @return Asset
     */
    public function setAssetcategory(\TC\DataPortalBundle\Entity\AssetCategory $assetcategory = null)
    {
        $this->assetcategory = $assetcategory;

        return $this;
    }

    /**
     * Get assetcategory
     *
     * @return \TC\DataPortalBundle\Entity\AssetCategory 
     */
    public function getAssetcategory()
    {
        return $this->assetcategory;
    }
    /**
     * @var point
     */
    private $location;

    /**
     * @var \TC\DataPortalBundle\Entity\Ward
     */
    private $ward;


    /**
     * Set location
     *
     * @param point $location
     * @return Asset
     */
    public function setLocation($location)
    {
    	$location = new Point(-73.7562317, 42.6525793);
    	
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return point 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set ward
     *
     * @param \TC\DataPortalBundle\Entity\Ward $ward
     * @return Asset
     */
    public function setWard(\TC\DataPortalBundle\Entity\Ward $ward = null)
    {
        $this->ward = $ward;

        return $this;
    }

    /**
     * Get ward
     *
     * @return \TC\DataPortalBundle\Entity\Ward 
     */
    public function getWard()
    {
        return $this->ward;
    }
    
    public function __toString()
    {
    	return $this->code;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if(!$this->getCreatedAt()) {
            $this->createdAt = new \DateTime();
        }
    }
}
