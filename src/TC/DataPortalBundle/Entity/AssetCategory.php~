<?php

namespace TC\DataPortalBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AssetCategory
 */
class AssetCategory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AssetCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return AssetCategory
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return AssetCategory
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    public function __toString()
    {
    	return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $kpicollections;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->kpicollections = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add kpicollections
     *
     * @param \TC\DataPortalBundle\Entity\AssetCategoryKPICollection $kpicollections
     * @return AssetCategory
     */
    public function addKpicollection(\TC\DataPortalBundle\Entity\AssetCategoryKPICollection $kpicollections)
    {
        $this->kpicollections[] = $kpicollections;

        return $this;
    }

    /**
     * Remove kpicollections
     *
     * @param \TC\DataPortalBundle\Entity\AssetCategoryKPICollection $kpicollections
     */
    public function removeKpicollection(\TC\DataPortalBundle\Entity\AssetCategoryKPICollection $kpicollections)
    {
        $this->kpicollections->removeElement($kpicollections);
    }

    /**
     * Get kpicollections
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getKpicollections()
    {
        return $this->kpicollections;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if(!$this->getCreatedAt()) {
            $this->createdAt = new \DateTime();
        }
    }

    
}
