<?php

namespace TC\DataPortalBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AssetReportType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weightageResults')
            ->add('dateOfInspection')
            ->add('userId')
            ->add('deviceId')
            ->add('asset')
            ->add('kpicollection')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'data_class' => 'TC\DataPortalBundle\Entity\AssetReport'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tc_dataportalbundle_assetreport';
    }
}
