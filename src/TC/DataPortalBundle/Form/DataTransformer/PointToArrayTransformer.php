<?php

namespace TC\DataPortalBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use CrEOF\Spatial\PHP\Types\Geometry\Point as Point;

class PointToArrayTransformer implements DataTransformerInterface
{
	

	
	public function transform($point)
	{
		if (null === $point) {
			return "";
		}

		return $point->toArray();
	}
	
	public function reverseTransform($array)
	{
		
		return $array;
	}

	
}
