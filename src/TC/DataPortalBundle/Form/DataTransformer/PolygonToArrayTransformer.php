<?php

namespace TC\DataPortalBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use CrEOF\Spatial\PHP\Types\Geometry\Polygon as Polygon;

class PolygonToArrayTransformer implements DataTransformerInterface
{
	

	
	public function transform($polygon)
	{
		if (null === $polygon) {
			return "";
		}

		return $polygon->toArray();
	}
	
	public function reverseTransform($array)
	{
		$polygon = new Polygon($array);
		return $polygon;
	}

	
}
