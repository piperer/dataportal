<?php

namespace TC\DataPortalBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use TC\DataPortalBundle\Form\DataTransformer\PointToArrayTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class PointType extends AbstractType
{
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new PointToArrayTransformer();
        $builder->addModelTransformer($transformer);
    }

    public function getName()
    {
        return 'point';
    }
	
}
