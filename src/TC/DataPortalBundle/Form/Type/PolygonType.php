<?php

namespace TC\DataPortalBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use TC\DataPortalBundle\Form\DataTransformer\PolygonToArrayTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class PolygonType extends AbstractType
{
	
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new PolygonToArrayTransformer();
        $builder->addModelTransformer($transformer);
    }

    public function getName()
    {
        return 'polygon';
    }
	
}
